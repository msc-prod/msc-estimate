//* this  file is require()d in /ALFA_DB_ADAPTER/config.js 

const app_name = require('path').basename(__dirname) //* the same as application folder name !!!

module.exports = {

    app_name,

    ALFA_DB_debug: true, //* send SQL inputParameters to front end for debug

    JSON_DB: { /* for NodeJS fs.read/write  
        used in  ...\ALFA_NODE\ALFA_DB_ADAPTER\index.js it has to be relative to index.js */
        dir: `../APPS/${app_name}`, //* where the JSON files are
    },

    MONGO_DB: {
        //* the path is relative to dir: ...\Node_API\ALFA_DB_ADAPTER
        requirePath: '../MONGO_DB/mongo-db-instance.js', // node_module mongo may be in other dir
        url: 'mongodb+srv://admin7:ESnU8ZLUQHoXATNl@cluster0.6whtc.mongodb.net/<dbname>?retryWrites=true&w=majority', // my Atlas account
        db_name: 'alfa_core', // Database Name in my Atlass account
    },

    MSSQL_DB: {
        
        //* the path is relative to dir: ...\Node_API\ALFA_DB_ADAPTER and is required for example in ...\Node_API\ALFA_DB_ADAPTER\MscSqlWebApi.js
        requirePath: '../MS_SQL/ms-sql-instance.js', // node_module ms-sql may be in other dir
        
        // NOT USED: mssqlConnectionUrl: 'mssql://collect:collect@MSC-DBS/MSSQLSERVER2019/ISOTECH_COLLECT_TRAIN_20210127'
    },
    
    MSC_SQL_WEB_API: {

        //* the path is relative to dir: ...\Node_API\ALFA_DB_ADAPTER and is required for example in ...\Node_API\ALFA_DB_ADAPTER\MscSqlWebApi.js
        requirePath: '../MS_SQL/ms-sql-instance.js', // node_module ms-sql may be in other dir
        
        // worked with mssql 6.3.1
        // mssqlConnectionUrl: 'mssql://collect:collect@MSC-DBS/MSSQLSERVER2019/ISOTECH_TRAIN_20210617',

        // mssql 7.2.1  
        // mssqlConnectString: 'Server=MSC-DBS\\MSSQLSERVER2019;Database=ISOTECH_TRAIN_20210617;User Id=collect;Password=collect;Encrypt=false;Trust Server Certificate=true',
        
        //* when dev14-pc is resolved
        mssqlConnectString: 'Server=dev14-pc;Database=ISOTECH_TRAIN_20210617;User Id=collect;Password=collect;Encrypt=false;Trust Server Certificate=true',
        
        //* when on VPN, the IP must be used
        // mssqlConnectString: 'Server=192.168.30.116;Database=ISOTECH_TRAIN_20210617;User Id=collect;Password=collect;Encrypt=false;Trust Server Certificate=true',

    }
}