@echo off
@echo ------------------------------------------------
@echo making Windows Service for AlfaNode API:
@echo name %1
@echo desc %2

node ./NODE-DAEMON %1 %2

:: error log may not be created yet, wait for it 5sec
@echo wait 5sec ...
ping 127.0.0.1 -n 6 > NUL

type ..\daemon\*.out.log
@echo -
@echo -- error log -----------------------------------
type ..\daemon\*.err.log
@echo ------------------------------------------------
@echo open Windows Services Manager (or ctrl+C to end)
@pause
services.msc