#!/bin/sh
# sudo chmod +x app-maint.sh 

case $1 in
    update)
        git stash
        git pull
        read -n1 -r -p "Press any key to continue, or ctrl-C to abort."
        
        # record commit ids to be used by undo-update
        commitID=$(git rev-parse --short HEAD)
        # prepend
        sed -i '1s/^/'$commitID'\n/' ../.app-maintenance/git-pull-commits


        node ./app-maint.js restore
        ;;
    undo-update)
        read -n1 -r -p "Undoing last commit, or ctrl-C to abort."
        
        # NOTE: only a git commit can be undone. 
        # read second line containing commitID before current
        lastCommit=$(sed -n '2p' ../.app-maintenance/git-pull-commits)

        git reset $lastCommit

#        node ./app-maint.js restore - is it usefull
        ;;
    build-info)
        cat build-info.json
        ;;
    test)
        # record commit ids to be used by undo-update
        commitID=$(git rev-parse --short HEAD)
        
        # prepend
        sed -i '1s/^/'$commitID'\n/' ../.app-maintenance/git-pull-commits
        ;;
    *)
        node ./app-maint.js $1 $2
        if [ $# -eq 0 ]
        then
            read -n1 -r -p "Press any key..."
        fi
        ;;
esac