:: this cmd is called from Task Scheduler or started manually
:: 
@echo off

if "%~d0"=="C:" (
    echo runing from C drive
) else (
    echo %~d0
    %~d0
)
:: cd %~dp0%

set nodeapiDir=%~dp0ALFA_NODE
:: echo %nodeapiDir%

cd %nodeapiDir%


if [%1] == [] (
    echo ------------------------------------------------
    echo manual start, DO NOT close this console window !
    node index.js console-win
) else (
    node index.js %1 %2
)
echo -
:: pause
