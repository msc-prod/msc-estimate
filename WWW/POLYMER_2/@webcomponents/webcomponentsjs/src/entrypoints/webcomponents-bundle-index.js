/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/
"use strict";import"./webcomponents-sd-ce-pf-index.js";const customElements=window.customElements;let shouldFlush=!1,flusher=null;function flushAndFire(){window.HTMLTemplateElement.bootstrap&&window.HTMLTemplateElement.bootstrap(window.document),flusher&&flusher(),shouldFlush=!0,window.WebComponents.ready=!0,document.dispatchEvent(new CustomEvent("WebComponentsReady",{bubbles:!0}))}customElements.polyfillWrapFlushCallback&&customElements.polyfillWrapFlushCallback(e=>{flusher=e,shouldFlush&&e()}),"complete"!==document.readyState?(window.addEventListener("load",flushAndFire),window.addEventListener("DOMContentLoaded",()=>{window.removeEventListener("load",flushAndFire),flushAndFire()})):flushAndFire();