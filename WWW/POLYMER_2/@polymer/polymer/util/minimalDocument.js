/**
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */
"use strict";const dom5=require("dom5"),parse5=require("parse5"),{Transform:Transform}=require("stream"),p=dom5.predicates;function onlyOneLicense(e){let o=dom5.nodeWalkAll(e,dom5.isCommentNode),t=!1;for(let e=0;e<o.length;e++){let n=o[e];-1===dom5.getTextContent(n).indexOf("@license")||t?dom5.remove(n):t=!0}}const blankRx=/^\s+$/,isBlankNode=p.AND(dom5.isTextNode,e=>blankRx.test(dom5.getTextContent(e)));class MinimalDocTransform extends Transform{constructor(){super({objectMode:!0})}_transform(e,o,t){let n=parse5.parse(String(e.contents),{locationInfo:!0}),r=dom5.query(n,p.AND(p.hasTagName("div"),p.hasAttr("by-polymer-bundler"),p.hasAttr("hidden"))),s=dom5.query(n,p.AND(p.hasTagName("meta"),p.hasAttrValue("charset","UTF-8")));s&&dom5.remove(s),dom5.removeNodeSaveChildren(r);let m=dom5.queryAll(n,p.hasTagName("script")),a=m[0],d=[dom5.getTextContent(a)];for(let e,o=1;o<m.length;o++)e=m[o],dom5.remove(e),d.push(dom5.getTextContent(e));dom5.setTextContent(a,d.join("")),onlyOneLicense(n),dom5.removeFakeRootElements(n),dom5.nodeWalkAll(n,isBlankNode).forEach(e=>dom5.remove(e)),e.contents=new Buffer(parse5.serialize(n)),t(null,e)}}module.exports=()=>new MinimalDocTransform;