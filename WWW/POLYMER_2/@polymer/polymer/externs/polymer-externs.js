/**
 * @fileoverview Externs for Polymer Pass and external Polymer API
 * @externs
 *
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */
let PolymerElementPropertiesMeta,PolymerElementProperties,PolymerInit=function(){};PolymerInit.prototype.is,PolymerInit.prototype.extends,PolymerInit.prototype.properties,PolymerInit.prototype.observers,PolymerInit.prototype.template,PolymerInit.prototype.hostAttributes,PolymerInit.prototype.listeners;let PolymerElementConstructor=function(){};PolymerElementConstructor.is,PolymerElementConstructor.extends,PolymerElementConstructor.properties,PolymerElementConstructor.observers,PolymerElementConstructor.template;let PropertiesMixinConstructor=function(){};function Polymer(e){}function JSCompiler_renameProperty(e,t){}function PolymerTelemetry(){}PropertiesMixinConstructor.properties,Polymer.sanitizeDOMValue,PolymerTelemetry.instanceCount,PolymerTelemetry.registrations,PolymerTelemetry._regLog,PolymerTelemetry.register,PolymerTelemetry.dumpRegistrations,Polymer.telemetry,Polymer.version;var PolymerElement=function(){};PolymerElement.prototype.created=function(){},PolymerElement.prototype.ready=function(){},PolymerElement.prototype.registered=function(){},PolymerElement.prototype.attached=function(){},PolymerElement.prototype.detached=function(){};