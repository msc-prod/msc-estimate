/**
 * @fileoverview Internal shared types for Polymer
 *
 * @license
 * Copyright (c) 2017 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */
function StampedTemplate(){}function NodeInfo(){}function TemplateInfo(){}function HTMLTemplateElementWithInfo(){}let LiteralBindingPart,MethodArg,MethodSignature,ExpressionBindingPart,BindingPart,Binding,PathInfo,TemplatizeOptions;function AsyncInterface(){}StampedTemplate.prototype.__noInsertionPoint,StampedTemplate.prototype.nodeList,StampedTemplate.prototype.$,StampedTemplate.prototype.templateInfo,NodeInfo.prototype.id,NodeInfo.prototype.events,NodeInfo.prototype.hasInsertionPoint,NodeInfo.prototype.templateInfo,NodeInfo.prototype.parentInfo,NodeInfo.prototype.parentIndex,NodeInfo.prototype.infoIndex,NodeInfo.prototype.bindings,TemplateInfo.prototype.nodeInfoList,TemplateInfo.prototype.nodeList,TemplateInfo.prototype.stripWhitespace,TemplateInfo.prototype.hasInsertionPoint,TemplateInfo.prototype.hostProps,TemplateInfo.prototype.propertyEffects,TemplateInfo.prototype.nextTemplateInfo,TemplateInfo.prototype.previousTemplateInfo,TemplateInfo.prototype.childNodes,TemplateInfo.prototype.wasPreBound,HTMLTemplateElementWithInfo.prototype._templateInfo,AsyncInterface.prototype.run,AsyncInterface.prototype.cancel;