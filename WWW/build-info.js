/* copy of server side json file created by alfa-build, editing here will not change values!  */
var buildInfo = {
    "gulpInfo": {
        "build": "22.4.27.15.28",
        "creator": "HP-Folio/admin7",
        "npmPackageVersion": "1.0.0",
        "appMainDir": "msc-estimate",
        "alfaBuilderVer": "22.1.20a"
    },
    "userInfo": {
        "Product info": {
            "Title": "msc-estimate",
            "Description": "",
            "Company": "Manufacturing Systems Corp."
        },
        "GATEWAY info": {
            "Version": ""
        },
        "Version info": {
            "Built By": "",
            "Version": "",
            "Date Created": ""
        },
        "Customer info": {
            "Customer": ""
        }
    },
    "gulpInfoProd": {
        "build": "22.4.27.15.33",
        "dateCreated": "Wed, 27 Apr 2022 19:33:49 GMT",
        "creator": "HP-Folio/admin7",
        "gulpFileLibVer": "21.5.14",
        "gulpArgs": null
    }
}
export default buildInfo