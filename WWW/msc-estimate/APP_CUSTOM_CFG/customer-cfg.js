
//*  this configuration is customer specific and can be in symlinked dir to be separated from core code

const alfaDb_customerSiteCFG = {

    //* forDEBUG, to check if proper config is loaded, also still used in alfa-ui-db-collection.mjs !!!
    app_dir: 'msc-estimate',

    SSRS_server: 'http://dev14-pc', //* SQL Server Reporting Services

    input_parsing: 'float', /*  user input value into <input type="number"> is either parsed to integer or float
                                this is not related to alfaDb, but since it is available in sessionStorage, it is here          */
    
    //* dbType for each collection
    
    // collection_dbType : {
    //     _default_: 'json-db',   //* for transaction getCollections(), all collections will use default !!!
    //                             //* getCollections() for ms-sql is using WEB_API_LOOKUPLIST_REQUEST

    //     DOC_ESTIMATE:  'json-db', //  
    //     DOC_QUOTATION: 'json-db', //  'ms-sql', // 
        
    //     // projects: 'json-db',
    //     PROJECT_MASTER: 'json-db',

    //     // customers: 'json-db', OLD Quotation Receipients
    //     PARTNER_MASTER_CUSTOMER: 'json-db', // 'json-db', //* Quotation Recipients, replacing customers.json 
    // },


    collection_dbType : {
        _default_: 'ms-sql',   //* for transaction getCollections(), all collections will use default !!!
                                //* getCollections() for ms-sql is using WEB_API_LOOKUPLIST_REQUEST

        DOC_ESTIMATE:  'ms-sql',
        DOC_QUOTATION: 'ms-sql',
        
        // projects: 'ms-sql',
        PROJECT_MASTER: 'ms-sql',

        // customers: 'ms-sql', OLD Quotation Receipients
        PARTNER_MASTER_CUSTOMER: 'ms-sql',  //* Quotation Recipients, replacing customers.json 
    },

}

export {alfaDb_customerSiteCFG}
